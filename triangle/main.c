#include <stdio.h>

int main(void)
{
	for (int i = 1; i <= 5; i++) {
		for (int j = 5 - i; j > 0; j--)
			printf(" ");

		for (int z = i * 2 -1; z > 0; z--)
			printf("*");

		printf("\n");
	}

	return 0;
}

#include <stdio.h>
#include <stddef.h>

struct test{
    char a;
    char b;
    int t;
    char z;
};

int main()
{
    printf("a offset    is %ld\n", offsetof(struct test, a));
    printf("b offset    is %ld\n", offsetof(struct test, b));
    printf("t offset    is %ld\n", offsetof(struct test, t));
    printf("whole size  is %ld\n", sizeof(struct test));
    return 0;
}

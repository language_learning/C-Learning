#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "cfg_info.h"

int main(void)
{
    int size = 0;

    size = sizeof(short);
    printf("%-20s = %d\n", "short", size);

    size = sizeof(int);
    printf("%-20s = %d\n", "int", size);

    size = sizeof(s_ate_gsm_flag);
    printf("%-20s = %d\n", "s_ate_gsm_flag", size);

    size = sizeof(s_ate_tdscdma_flag);
    printf("%-20s = %d\n", "s_ate_tdscdma_flag", size);

    size = sizeof(s_ate_flag);
    printf("%-20s = %d\n", "s_ate_flag", size);

    size = sizeof(s_wcn_ate_flag);
    printf("%-20s = %d\n", "s_wcn_ate_flag", size);

    size = sizeof(s_mtk_test_flag);
    printf("%-20s = %d\n", "s_mtk_test_flag", size);

    size = sizeof(PRODUCT_INFO);
    printf("%-20s = %d\n", "PRODUCT_INFO", size);


    printf("\n======================================\n\n");

    size = offsetof(s_ate_flag, sw_index);
    printf("%-20s = %d\n", "sw_index", size);

    size = offsetof(s_ate_flag, md_index);
    printf("%-20s = %d\n", "md_index", size);

    size = offsetof(s_ate_flag, gsm_flag);
    printf("%-20s = %d\n", "gsm_flag", size);

    size = offsetof(s_ate_flag, tdscdma_flag);
    printf("%-20s = %d\n", "tdscdma_flag", size);

    size = offsetof(s_ate_flag, wcdma_flag);
    printf("%-20s = %d\n", "wcdma_flag", size);

    size = offsetof(s_ate_flag, lte_flag);
    printf("%-20s = %d\n", "lte_flag", size);

    size = offsetof(s_ate_flag, cdma_flag);
    printf("%-20s = %d\n", "cdma_flag", size);

    size = offsetof(s_ate_flag, other_flag);
    printf("%-20s = %d\n", "other_flag", size);

    size = offsetof(s_ate_flag, nsft_flag);
    printf("%-20s = %d\n", "nsft_flag", size);

    size = offsetof(s_ate_flag, wireless_flag);
    printf("%-20s = %d\n", "wireless_flag", size);

    size = offsetof(s_ate_flag, reserved);
    printf("%-20s = %d\n", "reserved", size);

    return 0;
}

#include <stdio.h>
#include "test.h"

extern int global_var;
extern void fun1();
extern void fun2();

int main()
{
	printf("before main(), addr=ox%x, var=%d\n", &global_var, global_var);
	global_var = 3;
	printf("after  main(), var=%d\n", global_var);

	fun1();
	fun2();

	return 0;
}

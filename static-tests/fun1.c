#include <stdio.h>
#include "test.h"

extern int global_var;

void fun1()
{
	printf("before fun1(), addr=ox%x, var=%d\n", &global_var, global_var);
	global_var = 10;
	printf("after  fun1(), var=%d\n", global_var);
}

#include <stdio.h>
#include "test.h"

extern int global_var;

void fun2()
{
	printf("before fun2(), addr=ox%x, var=%d\n", &global_var, global_var);
	global_var = 20;
	printf("after  fun2(), var=%d\n", global_var);
}

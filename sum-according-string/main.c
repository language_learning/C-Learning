#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ERROR_NULL  (-99999999L)

long strToLong(char s)
{
    return (long) (s - '0');
}

long sum(char *string)
{
    long count = 0;
    char *tmp = string;
    long tmp_num = 0;
    long num[10] = {0};
    int n = 0;
    char symbol[10] = {'0'};
    int s = 0;

    if (string == NULL) {
        return ERROR_NULL;
    }

    printf("input:string=%s, strlen(string)=%ld\n", tmp, strlen(tmp));
    for (int i = 0; i < strlen(tmp); i++) {
        switch (tmp[i]) {
            case '-':
                symbol[s++] = '-';
                num[n++] = tmp_num;
                tmp_num = 0;
                break;
            case '+':
                symbol[s++] = '+';
                num[n++] = tmp_num;
                tmp_num = 0;
                break;
            default:
                tmp_num = tmp_num * 10 + strToLong(tmp[i]);
                break;
        }
    }
    num[n++] = tmp_num;

    count = num[0];
    for (int i = 0; i < 10; i++) {
        if (symbol[i] != '\0') {
            printf("\t%ld\n", num[i]);
            printf("\t%c\n", symbol[i]);
            if (symbol[i] == '-') {
                count -= num[i + 1];
                // printf("-----%ld\n", num[i + 1]);
            } else if (symbol[i] == '+') {
                count += num[i + 1];
                // printf("+++++%ld\n", num[i + 1]);
            }
        }
    }

    return count;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
        return -1;

    long count = sum(argv[1]);
    printf("sum=%ld\n", count);
    return 0;
}

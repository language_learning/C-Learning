To develop a calculator application.

Example input:
	a string as "12+45+34-32-54".

Required output:
	Calculate the result. 

This input string guarantee:
	1. Only includes '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '+'.
	2. The size of string is not 0.
	3. Not start with '-' or '+', not start with '-' or '+'.


Usage:
	./a.out 12+43+434-123

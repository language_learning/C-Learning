#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int *getUpTable(char *str)
{
    static int table[32] = {0};

    for (int i = 0; i < 26; i++) {
        for (int j = 0; j < strlen(str); j++) {
            if (str[j] == 'A' + i) {
                table[i] = 1;
            }
        }
    }

    return table;
}

int *getLowTable(char *str)
{
    static int table[32] = {0};

    for (int i = 0; i < 26; i++) {
        for (int j = 0; j < strlen(str); j++) {
            if (str[j] == 'a' + i) {
                table[i] = 1;
            }
        }
    }

    return table;
}

//int *getLongestSnake(char *str)
void getLongestSnake(int *str, int *index, int *max)
{
    int i = 0;
    int j = 0;
    int eachSnake[32] = {0};

    for (int i = 0; i < 32; i++) {
        if (str[i] == 1) {
            eachSnake[i] = 1;
            j = i + 1;
            for (; j < 32; j++) {
                if (str[j] == 1) {
                    eachSnake[i]++;
                } else {
                    break;
                }
            }
        }
    }

    for (int i = 0; i < 32; i++) {
        printf("===%c, %2d, %d\n", 'A' + i, i, eachSnake[i]);
        if (eachSnake[i] > *max) {
            *max = eachSnake[i];
            *index = i;
        }
    }
}

char *getFinalSnake(int index, int max)
{
    char *snake = malloc(2 * max + 1);

    for (int i = 0; i < max; i++) {
        snake[2 * i] = 'A' + index + i;
        snake[2 * i + 1] = 'a' + index + i;
    }

    return snake;
}

char *getSnakeNumber(char *str)
{
    int *upTable;
    int *lowTable;
    int snakeTable[32] = {0};
    int index = 0;
    int max = 0;

    if (str == NULL)
        return NULL;

    upTable = getUpTable(str);
    lowTable = getLowTable(str);

    for (int i = 0; i < 32; i++) {
        if (upTable[i] == 1 && lowTable[i] == 1) {
            snakeTable[i] = 1;
        }
    }

    getLongestSnake(snakeTable, &index, &max);
    return getFinalSnake(index, max);
}

int main(int argc, char *argv[])
{
    char *snake = NULL;
    if (argc != 2)
        return -1;

    // int i = getSnakeNumber(argv[1]);
    snake = getSnakeNumber(argv[1]);
    printf("OUT:\n%s\n", snake);

    free(snake);

    return 0;
}
